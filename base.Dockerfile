#+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-#
#       |S|c|i|e|n|c|e| |B|o|x|        #
#+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-#

# Docker file for base image

# Build and push to Docker registry with:
#   export RELEASE_VERSION=":v0"
#   docker build -t gitlab-registry.cern.ch/sciencebox/docker-images/parent-images/base${RELEASE_VERSION} -f base.Dockerfile .
#   docker login gitlab-registry.cern.ch
#   docker push gitlab-registry.cern.ch/sciencebox/docker-images/parent-images/base${RELEASE_VERSION}


FROM cern/cc7-base:20181210

MAINTAINER Enrico Bocchi <enrico.bocchi@cern.ch>


# ----- Set environment and language ----- #
ENV DEBIAN_FRONTEND noninteractive
ENV LANGUAGE en_US.UTF-8
ENV LANG en_US.UTF-8
ENV LC_ALL en_US.UTF-8

# ----- Remove Unwanted Packages ----- #
RUN yum -y remove \
      yum-autoupdate

# ----- Generic Tools ----- #
RUN yum -y install \
      bind-utils \
      git \
      less \
      lsof \
      sudo \
      which && \
    yum clean all && \
    rm -rf /var/cache/yum
      
# ----- Editors ----- #
RUN yum -y install \
      vim && \
    yum clean all && \
    rm -rf /var/cache/yum

# ----- Network Tools ----- #
RUN yum -y install \
      iftop \
      net-tools \
      tcpdump \
      telnet \
      wget && \
    yum clean all && \
    rm -rf /var/cache/yum

